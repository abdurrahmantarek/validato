<?php 

require "./Rules.php";
require "./Validato.php";


$validato = new Validato();

$inputs = [
	'name' => 'tarik',
	'phone' => '321312312313'
];

$rules = [
	'name' => 'string|required',
	'phone' => 'integer|required'
];

$validato->make($inputs, $rules);

var_dump($validato->errors);