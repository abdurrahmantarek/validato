
# **Validato** is a simple validation class that structured to be easy to use 


# How it works 

```
$validato = new Validato();

//assign input name to its validation rules spreated by | 
$rules = [

    'input1' => 'string|required',
    'input2' => 'integer|required',
];

//$inputs here might be your $_REQUEST 
//make validate 
$validato->make($inputs, $rules);

if($validato->fails()) {
    //thats mean every things is'nt validate do something
}

if($validato->passed()) {
    //thats mean every things is validate and do something
}

```

in [test.php](test.php) you could find a sample of how it works