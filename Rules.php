<?php 

abstract class Rules
{

	public function string($input, $value)
	{
		return is_string($value) ? false : "This field " .$input . " must be string";
	}

	public function required($input, $value)
	{
		return !empty($value) ? false : "This field " .$input . " is required";
	}

	public function integer($input, $value)
	{
		return is_integer($value) ? false : "This field " .$input . " must be interger";
	}
}