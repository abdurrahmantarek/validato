<?php 

class Validato extends Rules
{

	public $errors = [];

	private $inputRules = [];

	public function make(array $inputs, array $rules)
	{

		foreach($rules AS $input => $rule) {

			$value = $inputs[$input];

			$this->setInputRules($rule);

			foreach($this->inputRules AS $inputRule) {

				if(method_exists($this, $inputRule)) {

					$result = call_user_func_array([$this, $inputRule], [$input, $value]);

					if($result) {
						//add error
						$this->errors[] = $result;
					}

				}else {

					throw new Exception("There's no rule named" . $inputRule);
				}
			}

			
		}
	}

	private function setInputRules($rule)
	{
		preg_match_all("/[\w\s]+/", $rule, $match);

		$this->inputRules = $match[0];
	}

	public function fails ()
	{

		return !empty($this->errors) ? true : false;
	} 

	public function passed ()
	{
		return empty($this->errors) ? true : false;
	}
}